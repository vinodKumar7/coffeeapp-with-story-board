//
//  LoyaltyViewController.m
//  CoffeeApp
//
//  Created by vairat on 27/03/15.
//  Copyright (c) 2015 Vairat. All rights reserved.
//

#import "LoyaltyViewController.h"
#import "LoyaltyDetailViewController.h"
#import "ASIFormDataRequest.h"
#import "LoyalityTableViewCell.h"
#import "AppDelegate.h"
#import "AsyncImageView.h"
#import "KeyboardViewController.h"

@interface LoyaltyViewController (){
    
    ASIFormDataRequest *productFetchRequest;
    
    
    ProductListParser *productsXMLParser;
    ProductDataParser *productDataXMLParser;
    Product *currentProduct;
    AppDelegate *appDelegate;
    ASIFormDataRequest *ImageFetchRequest;
    
    AsyncImageView *asyncImageObj;
}

@property (strong, nonatomic)AsyncImageView *asyncImageObj;
@end


@implementation LoyaltyViewController
@synthesize loyaltyTableView, productsList,asyncImageObj,productImagesArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    productImagesArray = [[NSMutableArray alloc]init];
    
    self.title = @"Coffee App";
    self.view.backgroundColor = [UIColor lightGrayColor];
    
    NSString *urlString =@"http://184.107.152.53/app/webroot/newapp/get_loyalty_offers.php?cid=24";// @"http://www.myrewards.com.au/app/webroot/newapp/get_offers_by_merchant.php?cid=1";
    NSURL *url = [NSURL URLWithString:urlString];
    
    productFetchRequest = [ASIFormDataRequest requestWithURL:url];
    [productFetchRequest setDelegate:self];
    [productFetchRequest startAsynchronous];
    
    [self createTableView];
    
//    appDelegate.activityIndicatorBaseView = [[UIView alloc]initWithFrame:self.view.bounds];
//    appDelegate.activityIndicatorBaseView.backgroundColor = [[UIColor grayColor]colorWithAlphaComponent:.7];
//    [self.view addSubview:appDelegate.activityIndicatorBaseView];
//    
//    [appDelegate activityIndicatorView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Programmatically Creating Views
-(void)createTableView
{
    //    loyaltyTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) style:UITableViewStylePlain];
    //
    //    loyaltyTableView.dataSource = self;
    //    loyaltyTableView.delegate = self;
    //    [self.view addSubview:loyaltyTableView];
    
    [self setUpView];
    
    NSDictionary *viewsDictionary = @{@"loyaltyTableView":self.loyaltyTableView};
    NSDictionary *metrics = @{@"vSpacing":@0, @"hSpacing":@0};
    
    // Define the view Position and automatically the Size
    NSArray *constraint_POS_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-vSpacing-[loyaltyTableView]-vSpacing-|"
                                                                        options:0
                                                                        metrics:metrics
                                                                          views:viewsDictionary];
    
    NSArray *constraint_POS_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-hSpacing-[loyaltyTableView]-hSpacing-|"
                                                                        options:0
                                                                        metrics:metrics
                                                                          views:viewsDictionary];
    
    [self.view addConstraints:constraint_POS_V];
    [self.view addConstraints:constraint_POS_H];
    
}
-(void)setUpView
{
//    loyaltyTableView = [UITableView new];
//    self.loyaltyTableView.translatesAutoresizingMaskIntoConstraints = NO;
//    loyaltyTableView.dataSource = self;
//    loyaltyTableView.delegate = self;
//    // loyaltyTableView.separatorColor=[UIColor clearColor];
//    [self.view addSubview:loyaltyTableView];
}

#pragma mark- TableView DataSource and Delegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [productsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    
    static NSString *cellIdentifier = @"loyaltyDetailCell";
    
    LoyalityTableViewCell *cell;
    
    if (cell == nil)
        cell = (LoyalityTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        
    
    Product *p = [productsList objectAtIndex:indexPath.row];
    cell.lblTitle.text = p.productName;
    cell.lblSubTitle.text = p.productOffer;
    
    
    
    
    //cell.bgCell.layer.borderWidth=2.0;
    //cell.bgCell.layer.borderColor=[UIColor grayColor].CGColor;
    
    AsyncImageView *async = [[AsyncImageView alloc]initWithFrame:CGRectMake(5,20, 85, 65)];
    
    [async loadImageFromURL:[NSURL URLWithString:p.carouselImgLink]];
     NSLog(@"asynImg Array %@",async.imgArray);
    
    [cell addSubview:async];
    [productImagesArray addObject:async];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}



#pragma mark- RequestFinished and RequestFailed

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    
    if(request == productFetchRequest){
        NSLog(@"========>> Product List:: %@",[request responseString]);
        NSXMLParser *productsParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        productsXMLParser = [[ProductListParser alloc] init];
        productsXMLParser.delegate = self;
        productsParser.delegate = productsXMLParser;
        [productsParser parse];
        
        
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    UIAlertView* alert_view = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                         message:@"Server Busy"
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
    [alert_view show];
    
    NSLog(@"Product Result:: %@",[request responseString]);
    
}

#pragma mark- Calling Parsing Methods
- (void)parsingProductListFinished:(NSArray *)prodcutsListLocal
{
    NSLog(@"product name %@",prodcutsListLocal);
    
    if (!productsList)
        
        productsList = [[NSMutableArray alloc] initWithArray:prodcutsListLocal];
    
    else
        
        [productsList addObjectsFromArray:prodcutsListLocal];
    
    
    [loyaltyTableView reloadData];
    
//    [appDelegate.activityIndicatorBaseView removeFromSuperview];
}

- (void)parsingProductListXMLFailed
{
    //NSLog(@"Product list updated failed ");
}


- (void)parsingProductDataFinished:(Product *)product
{
    currentProduct = product;
    NSLog(@"===currentProduct redeemTarget::%@", currentProduct.redeemTarget);
    // [productsList replaceObjectAtIndex:page_no withObject:product];
    
    //[self updateUIWithProductDetails:product];
    
    
    
    //    productImageRequest = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:currentProduct.productImgLink]];
    //    productImageRequest.delegate = self;
    //    [productImageRequest startAsynchronous];
    
}

- (void)parsingProductDataXMLFailed
{
    
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    //asyncImageObj = [[AsyncImageView alloc]init];
   
    if ([segue.identifier isEqualToString:@"loyaltyDetail"])
    {
        NSIndexPath *indexPath = [self.loyaltyTableView indexPathForSelectedRow];
        Product *pro = [productsList objectAtIndex:indexPath.row];
        
      
        //LoyaltyDetailViewController *loyaltyDetailController=[self.storyboard instantiateViewControllerWithIdentifier:@"loyaltyDetail"];
       
        LoyaltyDetailViewController *loyaltyDetailController = segue.destinationViewController;
        
        loyaltyDetailController.product_id = pro.productId;
        loyaltyDetailController.currentProduct=pro;
      
        loyaltyDetailController.productImage = [appDelegate.imagesArray objectAtIndex:indexPath.row] ;
        
        NSLog(@"product images Array si %@  loyaltyDetailController.currentProduct is %@",loyaltyDetailController.productImage,loyaltyDetailController.currentProduct);
    
    
    }
}


@end
