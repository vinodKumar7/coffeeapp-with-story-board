//
//  LoyaltyDetailViewController.m
//  CoffeeApp
//
//  Created by vairat on 28/03/15.
//  Copyright (c) 2015 Vairat. All rights reserved.
//

#import "LoyaltyDetailViewController.h"
#import "CollectionViewManager.h"
#import "KeyboardViewController.h"
#import "AppDelegate.h"

@interface LoyaltyDetailViewController (){
    
    BOOL buttonPressed;
    AppDelegate *appDelegate;
    Product *currentProduct;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong,nonatomic) CollectionViewManager *collectionViewManager;
@property (strong,nonatomic) KeyboardViewController *keyboardViewControllerObj;


@end

@implementation LoyaltyDetailViewController
@synthesize keyboardViewControllerObj;
@synthesize productImage, product_id,currentProduct;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
   
//        self.title = @"Product Detail";
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    //self.navigationController.navigationBar.hidden = NO;
    //self.collectionView.contentInset=UIEdgeInsetsMake(0, 0, 0, 0);
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    //self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    
    [self.navigationController.navigationBar setTranslucent:NO];
    
    self.title = @"Product Detail";

    NSLog(@"productId is...... %@",product_id);
    
    _collectionViewManager = [[CollectionViewManager alloc]init];
    self.collectionViewManager.product_ID = product_id;
    self.collectionViewManager.collectionView = self.collectionView;
    
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
     NSLog(@"Before keyboardViewControllerObj.product_ID is %@ ",keyboardViewControllerObj.product_ID);
    
    
}
- (void)viewWillAppear:(BOOL)animated
{
    
    
    
    NSLog(@"product image is %@ ",productImage);
    _collectionViewManager.proImage = productImage;
    appDelegate.activityIndicatorBaseView = [[UIView alloc]initWithFrame:self.view.bounds];
    appDelegate.activityIndicatorBaseView.backgroundColor = [[UIColor grayColor]colorWithAlphaComponent:.7];
//    [self.view addSubview:appDelegate.activityIndicatorBaseView];
    [appDelegate activityIndicatorView];
    
    if (buttonPressed == YES) {
        buttonPressed = NO;
        [_collectionViewManager redeemButtonPressed];
    }
    /*UIStoryboardSegue *segue;
    KeyboardViewController *loyaltyDetailController = segue.destinationViewController;
    
    loyaltyDetailController.product_ID = product_id;
     NSLog(@"LDDDDD keyboardViewControllerObj.product_ID is %@ ",keyboardViewControllerObj.product_ID);
    keyboardViewControllerObj = [self.storyboard instantiateViewControllerWithIdentifier:@"keyboard"];
    NSLog(@"LDC product image is %@ keyboardViewControllerObj %@",product_id,keyboardViewControllerObj);
    //
    //
    keyboardViewControllerObj.product_ID = product_id;
    NSLog(@"keyboardViewControllerObj.product_ID is %@ ",keyboardViewControllerObj.product_ID);*/
}


- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (IBAction)redeemBtnPressed:(id)sender {
    
    buttonPressed = YES;
    [_collectionViewManager lazyScrollReposition];
    
    
//    KeyboardViewController *kvc = (KeyboardViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"keyboard"];
//    //menu is only an example
//    kvc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    [self presentViewController:kvc animated:YES completion:nil];
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
//    if ([segue.identifier isEqualToString:@"keyboard"])
//    {
//        keyboardViewControllerObj = [[KeyboardViewController alloc]initWithNibName:@"KeyboardViewController" bundle:nil];
//        keyboardViewControllerObj.product_ID = product_id;
//        
//        [self presentViewController:keyboardViewControllerObj animated:NO completion:nil];
//    }
    
//    [self performSegueWithIdentifier:@"keyboard" sender:self];
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
//    KeyboardViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"keyboard"];
//    [self.navigationController pushViewController:vc animated:YES];
    
   // UIStoryboard *storyboard = self.storyboard;
   // KeyboardViewController *kvc = [storyboard instantiateViewControllerWithIdentifier:@"keyboard"];
    
   // [self presentViewController:kvc animated:YES completion:NULL];
    KeyboardViewController *kVC = segue.destinationViewController;
    
    kVC.product_ID = product_id;
    kVC.productImage=productImage.image;
    kVC.productName=currentProduct.productName;
    kVC.productOffer=currentProduct.productOffer;
   
    
    NSLog(@"prepare for segue kVC.product_ID is%@  currentProduct.productName is%@ currentProduct.productOffer is%@  ",kVC.productImg,currentProduct.productName,currentProduct.productOffer);
    
}



@end
