//
//  LoyaltyViewController.m
//  CoffeeApp
//
//  Created by vairat on 27/03/15.
//  Copyright (c) 2015 Vairat. All rights reserved.
//

#import "LoyaltyViewController.h"
#import "LoyaltyDetailViewController.h"
#import "ASIFormDataRequest.h"
#import "LoyalityTableViewCell.h"
#import "CoffeeApp.pch"
#import "Product.h"

@interface LoyaltyViewController (){
    
    ProductListParser  *productsXMLParser;
}

@property (strong, nonatomic) UIView *activityIndicatorBaseView;
@end


@implementation LoyaltyViewController
@synthesize loyaltyTableView, productsList, productImagesArray;
@synthesize activityIndicatorBaseView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Loyalty Products";
    self.view.backgroundColor = [UIColor lightGrayColor];
    
    productImagesArray = [[NSMutableArray alloc]init];
    
//    NSString *urlString = @"http://www.myrewards.com.au/app/webroot/newapp/get_offers_by_merchant.php?cid=1";
    
    NSString *urlString = @"http://184.107.152.53/app/webroot/newapp/get_loyalty_offers.php?cid=24"; //@"http://www.myrewards.com.au/app/webroot/newapp/get_offers_by_merchant.php?cid=1";
    NSURL *url = [NSURL URLWithString:urlString];
    
    ASIFormDataRequest *productFetchRequest = [ASIFormDataRequest requestWithURL:url];
    [productFetchRequest setDelegate:self];
    [productFetchRequest startAsynchronous];
   
    [self setUpActivityIndicatorBaseView];
}

- (void)viewWillAppear:(BOOL)animated
{
    
}

-(void) setUpActivityIndicatorBaseView
{
    activityIndicatorBaseView = [[UIView alloc]initWithFrame:self.view.bounds];
    activityIndicatorBaseView.backgroundColor = [[UIColor grayColor]colorWithAlphaComponent:.7];
    [self.view addSubview:activityIndicatorBaseView];
    
    [self activityIndicatorView];
}

-(void) activityIndicatorView
{
    CGRect frame = CGRectMake(self.view.bounds.size.width/3, self.view.bounds.size.height/2, 120, 40);
    UILabel *lblLoading = [[UILabel alloc]initWithFrame:frame];
    lblLoading.backgroundColor = [UIColor clearColor];
    [lblLoading setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15]];
    lblLoading.text = @"Loading...";
    lblLoading.textAlignment = NSTextAlignmentCenter;
    [lblLoading setTextColor:[UIColor blackColor]];
    
    [activityIndicatorBaseView addSubview:lblLoading];
    
    CGRect frameActivity = CGRectMake(self.view.bounds.size.width/3.5, self.view.bounds.size.height/2, 40, 40);
    UIActivityIndicatorView  *myActivityIndicatorView = [[UIActivityIndicatorView alloc]initWithFrame:frameActivity];
    [activityIndicatorBaseView addSubview:myActivityIndicatorView];
    
    [myActivityIndicatorView startAnimating];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- RequestFinished and RequestFailed

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    NSXMLParser *productsParser = [[NSXMLParser alloc] initWithData:[request responseData]];
    productsXMLParser = [[ProductListParser alloc] init];
    productsXMLParser.delegate = self;
    productsParser.delegate = productsXMLParser;
    [productsParser parse];
    
    
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    UIAlertView* alert_view = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                         message:@"Server Busy"
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
    [alert_view show];
    
    
}


#pragma mark- TableView DataSource and Delegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [productsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    static NSString *cellIdentifier = @"loyaltyDetailCell";
    
    LoyalityTableViewCell *cell;
    
    if (cell == nil)
        cell = (LoyalityTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    cell.tag = indexPath.row;
    Product *pro = [productsList objectAtIndex:indexPath.row];
    cell.lblTitle.text = pro.productName;
    cell.lblSubTitle.text = pro.productOffer;
    
    
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^(void) {
        
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:pro.carouselImgLink]];
        
        UIImage* image = [[UIImage alloc] initWithData:imageData];
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (cell.tag == indexPath.row) {
                    cell.product_ImageView.image = image;
                    [productImagesArray addObject:image];
                    [cell setNeedsLayout];
                    
                    [activityIndicatorBaseView removeFromSuperview];
                }
            });
        }
    });
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}





#pragma mark- Calling Parsing Methods
- (void)parsingProductListFinished:(NSArray *)prodcutsListLocal
{
    //NSLog(@"product name %@",prodcutsListLocal);
    
    if (!productsList)
        
        productsList = [[NSMutableArray alloc] initWithArray:prodcutsListLocal];
    
    else
        
        [productsList addObjectsFromArray:prodcutsListLocal];
    
    
    [loyaltyTableView reloadData];
    
    
}

- (void)parsingProductListXMLFailed
{
    //NSLog(@"Product list updated failed ");
}





#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    if ([segue.identifier isEqualToString:@"loyaltyDetail"])
    {
        NSIndexPath *indexPath = [self.loyaltyTableView indexPathForSelectedRow];
        Product *pro = [productsList objectAtIndex:indexPath.row];
        
        
        
        
        LoyaltyDetailViewController *loyaltyDetailController = segue.destinationViewController;
        loyaltyDetailController.product_id   = pro.productId;
        loyaltyDetailController.product_Name = pro.productName;
        loyaltyDetailController.productImage = [productImagesArray objectAtIndex:indexPath.row];
        
    }
}


@end
