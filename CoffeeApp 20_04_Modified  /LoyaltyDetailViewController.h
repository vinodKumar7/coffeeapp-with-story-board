//
//  LoyaltyDetailViewController.h
//  CoffeeApp
//
//  Created by vairat on 28/03/15.
//  Copyright (c) 2015 Vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductListParser.h"
#import "ProductDataParser.h"

@interface LoyaltyDetailViewController : UIViewController

@property(nonatomic, strong)UIImage *productImage;
@property(nonatomic, strong)NSString *product_id;
@property(nonatomic, strong)NSString *product_Name;

@property (strong, nonatomic) IBOutlet UIButton *redeemNowOutlet;

@end
