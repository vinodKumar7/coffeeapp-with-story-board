//
//  LoyaltyViewController.h
//  CoffeeApp
//
//  Created by vairat on 27/03/15.
//  Copyright (c) 2015 Vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductListParser.h"


@interface LoyaltyViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, ProductListXMLParserDelegate>
{
    
}

@property (nonatomic, strong)IBOutlet UITableView *loyaltyTableView;
@property (strong, nonatomic) NSMutableArray *productsList;
@property (strong, nonatomic) NSMutableArray *productImagesArray;

@end