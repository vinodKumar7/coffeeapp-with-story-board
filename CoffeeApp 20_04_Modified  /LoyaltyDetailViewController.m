//
//  LoyaltyDetailViewController.m
//  CoffeeApp
//
//  Created by vairat on 28/03/15.
//  Copyright (c) 2015 Vairat. All rights reserved.
//

#import "LoyaltyDetailViewController.h"
#import "CollectionViewManager.h"
#import "KeyboardViewController.h"
#import <AdSupport/ASIdentifierManager.h>
@interface LoyaltyDetailViewController (){
    
    BOOL buttonPressed;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong,nonatomic) CollectionViewManager *collectionViewManager;
@property (strong,nonatomic) KeyboardViewController *keyboardViewControllerObj;


@end

@implementation LoyaltyDetailViewController
@synthesize keyboardViewControllerObj;
@synthesize productImage, product_id, product_Name;
@synthesize redeemNowOutlet;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        

    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    
    
    [self.navigationController.navigationBar setTranslucent:NO];
    
    self.title = @"Product Detail";
    
    NSLog(@"productId is...... %@",product_id);
    
    _collectionViewManager = [[CollectionViewManager alloc]init];
    self.collectionViewManager.product_ID = product_id;
    _collectionViewManager.proImage = productImage;
    _collectionViewManager.deviceID = [self deviceUDID];
    self.collectionViewManager.collectionView = self.collectionView;
    
}

-(void) viewWillAppear:(BOOL)animated
{
    [self setUpActivityIndicatorBaseView];
    
    if (buttonPressed == YES) {
        buttonPressed = NO;
        [_collectionViewManager redeemButtonPressed];
    }
}



- (BOOL)prefersStatusBarHidden
{
    return NO;
}

-(void) setUpActivityIndicatorBaseView
{
    _collectionViewManager.activityIndicatorBaseView = [[UIView alloc]initWithFrame:self.view.bounds];
    _collectionViewManager.activityIndicatorBaseView.backgroundColor = [[UIColor grayColor]colorWithAlphaComponent:.7];
    [self.view addSubview:_collectionViewManager.activityIndicatorBaseView];
    
    [self activityIndicatorView];
}

-(void) activityIndicatorView
{
    CGRect frame = CGRectMake(self.view.bounds.size.width/3, self.view.bounds.size.height/2, 120, 40);
    UILabel *lblLoading = [[UILabel alloc]initWithFrame:frame];
    lblLoading.backgroundColor = [UIColor clearColor];
    [lblLoading setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15]];
    lblLoading.text = @"Loading...";
    lblLoading.textAlignment = NSTextAlignmentCenter;
    [lblLoading setTextColor:[UIColor blackColor]];
    
    [_collectionViewManager.activityIndicatorBaseView addSubview:lblLoading];
    
    CGRect frameActivity = CGRectMake(self.view.bounds.size.width/3.5, self.view.bounds.size.height/2, 40, 40);
    UIActivityIndicatorView  *myActivityIndicatorView = [[UIActivityIndicatorView alloc]initWithFrame:frameActivity];
    [_collectionViewManager.activityIndicatorBaseView addSubview:myActivityIndicatorView];
    
    [myActivityIndicatorView startAnimating];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString *)deviceUDID
{
    
    NSString *uid;
    if([ [ UIScreen mainScreen ] bounds ].size.height == 568)
    {
        uid= [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        
    }
    else
    {
        uid = [self advertisingIdentifier];
        if ([uid isKindOfClass:NULL]) {
            uid = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
        }
        
    }
    
    
    NSLog(@"UDID is %@ ",uid);
    
    return uid;
}
- (NSString *) advertisingIdentifier
{
    if (!NSClassFromString(@"ASIdentifierManager")) {
        SEL selector = NSSelectorFromString(@"uniqueIdentifier");
        if ([[UIDevice currentDevice] respondsToSelector:selector]) {
            return [[UIDevice currentDevice] performSelector:selector];
        }
        
    }
    return [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"KeyBoardVC"])
    {
        buttonPressed = YES;
        [_collectionViewManager lazyScrollReposition];

        KeyboardViewController *kvc = segue.destinationViewController;
        kvc.product_ID    = product_id;
        kvc.product_Image = productImage;
        kvc.product_Name  = product_Name;
        kvc.deviceID = [self deviceUDID];
        
        kvc.redeemPassword = _collectionViewManager.redeem_psw;
        kvc.multipleRedeemCount = _collectionViewManager.selectedStampCount;
        
        
    }
}



@end
