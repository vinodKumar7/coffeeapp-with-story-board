//
//  CollectionViewManager.h
//  PDKTStickySectionHeadersCollectionViewLayoutDemo
//
//  Created by Daniel García on 31/12/13.
//  Copyright (c) 2013 Produkt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PDKTStickySectionHeadersCollectionViewLayout.h"

#import "ProductDataParser.h"
#import "RedeemXMLparser.h"
#import "MerchantListParser.h"
#import "AddressCell.h"

@interface CollectionViewManager : NSObject<UICollectionViewDataSource,UICollectionViewDelegate,PDKTStickySectionHeadersCollectionViewLayoutDelegate,ProductXMLParserDelegate,RedeemXMLparserDelegate, MerchantXMLParserDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>
{
    int phnoCount;
    NSMutableArray *phMutableArray;
    NSArray *phArray;
    
    CLLocationCoordinate2D Location;
    CLLocationCoordinate2D userLocation;
    CLLocationManager *locationManager;
}

@property (weak,nonatomic) UICollectionView *collectionView;
@property (strong,nonatomic) NSString *product_ID;
@property (strong, nonatomic) IBOutlet AddressCell *cCell;
@property(nonatomic, retain) UIImage *proImage;
@property (strong, nonatomic) NSString *deviceID;

@property (strong, nonatomic) UIView *activityIndicatorBaseView;
@property (strong, nonatomic) NSString *redeem_psw;
@property (strong, nonatomic) NSString *selectedStampCount;


- (void)collectionView:(UICollectionView *)myCollectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath layout:(UICollectionViewLayout*)collectionViewLayout;

-(void)redeemButtonPressed;
-(void)lazyScrollReposition;
@end
