//
//  RedeemXMLparser.m
//  CWE
//
//  Created by vairat on 21/08/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import "RedeemXMLparser.h"
#import "Redeem.h"
#import "AppDelegate.h"

@interface RedeemXMLparser()
{
    NSMutableArray *categoryList;
    NSMutableString *charString;
    Redeem *redeem;
}
@property (nonatomic, strong) NSMutableArray *categoryList;

@end
@implementation RedeemXMLparser
@synthesize delegate;
@synthesize categoryList;

- (void)parserDidStartDocument:(NSXMLParser *)parser {
   // categoryList = [[NSMutableArray alloc] init];
}


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    charString = nil;
    
    if ([elementName isEqualToString:@"root"]) {
        
        if (redeem) {
            redeem = nil;
        }
        
        redeem = [[Redeem alloc] init];
    }
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if (!charString) {
        charString = [[NSMutableString alloc] initWithString:string];
    }
    else {
        [charString appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    NSString *finalString = [charString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
     [categoryList addObject:redeem];
     if ([elementName isEqualToString:@"count"])
     {
        redeem.count = finalString;
    }
     else if ([elementName isEqualToString:@"root"]) {
         [delegate parsingcountFinished:redeem];
         NSLog(@" redeem count is %@ ", redeem.count);
     }
    
   // NSLog(@"count is... %@ ",finalString);
    //NSLog(@"catogery list %@",[categoryList objectAtIndex:0]);
   }

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    [delegate RedeemXMLparsingFailed];
    NSLog(@"error occured..");
}



@end
