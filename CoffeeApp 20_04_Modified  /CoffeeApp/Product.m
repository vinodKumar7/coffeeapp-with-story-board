//
//  Product.m
//  GrabItNow
//
//  Created by MyRewards on 12/23/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import "Product.h"

@implementation Product

@synthesize productImgLink;
@synthesize appImageExtension;
@synthesize carouselImgLink;
@synthesize productName;
@synthesize productId;
@synthesize productDesciption;
@synthesize productOffer;
@synthesize productText;

@synthesize pin_type;
@synthesize termsAndConditions;
@synthesize contactMerchantName,contactSuburb,contactState,contactPostcode,contactlastName,contactFirstName,contactCountry;

@synthesize imageDoesntExist;

@synthesize displayImagePrefix;
@synthesize imageExtension;

@synthesize phone;
@synthesize mobile;
@synthesize websiteLink;

@synthesize productDetailedDesciption;
@synthesize productTermsAndConditions;

@synthesize coordinate;
@synthesize hotoffer_extension;


@synthesize merchantId;
@synthesize merchantLogoExtention;
@synthesize quantity;

@synthesize redeemTarget,currentRedeemCount;
@synthesize redeemPassword;

@synthesize redeemTarget,currentRedeemCount,redeemPassword;


- (id)init {
    self = [super init];
    
    if (self) {
        imageDoesntExist = NO;
    }
    
    return self;
}

@end


