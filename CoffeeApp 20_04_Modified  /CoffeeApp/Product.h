//
//  Product.h
//  GrabItNow
//
//  Created by MyRewards on 12/23/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
@interface Product : NSObject
{
    NSString *productImgLink;
    NSString *appImageExtension;
    NSString *productName;
    NSString *productId;
    NSString *productDesciption;
    NSString *productOffer;
    NSString *productText;
    NSString *termsAndConditions;
   // UIImage *productImage;
    BOOL imageDoesntExist;
    
    // Contact Properties.
    NSString *contactMerchantName;
    NSString *contactFirstName;
    NSString *contactlastName;
    NSString *contactTitle;
    NSString *contactAddress1;
    NSString *contactAddress2;

    NSString *phone;
    NSString *mobile;
    NSString *websiteLink;
    
    NSString *contactSuburb;
    NSString *contactState;
    NSString *contactPostcode;
    NSString *contactCountry;
    
    NSString *displayImagePrefix;
    NSString *imageExtension;
    NSString *pin_type;
    NSString *hotoffer_extension;
     NSString *quantity;
    
    // Other properties
    NSString *productDetailedDesciption;
    NSString *productTermsAndConditions;
    CLLocationCoordinate2D coordinate;
    
    
    
    //image properties
    NSString *merchantId;
    NSString *merchantLogoExtention;
    NSString *redeemTarget;
    NSString *currentRedeemCount;
    NSString *redeemPassword;
}

@property (nonatomic, strong) NSString *productImgLink;
@property (nonatomic, strong) NSString *appImageExtension;
@property (nonatomic, strong) NSString *carouselImgLink;
@property (nonatomic, strong) NSString *productName;
@property (nonatomic, strong) NSString *productId;
@property (nonatomic, strong) NSString *productDesciption;
@property (nonatomic, strong) NSString *productOffer;
@property (nonatomic, strong) NSString *productText;
@property (nonatomic, strong) NSString *termsAndConditions;
//@property (nonatomic, strong) UIImage *productImage;
@property (nonatomic, strong) NSString *redeemTarget;
@property (nonatomic, strong) NSString *redeemPassword;
// This property exactly is not Product's requirement.
// This is for image presenting logic.
@property (nonatomic, readwrite) BOOL imageDoesntExist;


@property (nonatomic, strong) NSString *productDetailedDesciption;
@property (nonatomic, strong) NSString *productTermsAndConditions;

@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *websiteLink;
@property (nonatomic, strong) NSString *pin_type;
@property (nonatomic, strong) NSString *hotoffer_extension;
@property (nonatomic, readwrite) CLLocationCoordinate2D coordinate;


@property (nonatomic, strong) NSString *contactMerchantName;
@property (nonatomic, strong) NSString *contactFirstName;
@property (nonatomic, strong) NSString *contactlastName;
@property (nonatomic, strong) NSString *contactSuburb;
@property (nonatomic, strong) NSString *contactState;
@property (nonatomic, strong) NSString *contactPostcode;
@property (nonatomic, strong) NSString *contactCountry;

@property (nonatomic, strong) NSString *displayImagePrefix;
@property (nonatomic, strong) NSString *imageExtension;

@property (nonatomic, strong) NSString *merchantId;
@property (nonatomic, strong) NSString *merchantLogoExtention;
@property (nonatomic, strong) NSString *quantity;
@property (nonatomic, strong) NSString *currentRedeemCount;
@property (nonatomic, strong) NSString *redeemPassword;

@end





/*
 
 <mname>Metung Holiday Villas </mname>
 <contact_first_name>Garry</contact_first_name>
 <contact_last_name>Avage</contact_last_name>
 <contact_title>Mr</contact_title>
 <contact_position>Manager</contact_position>
 <mail_address1>Cnr Mairburn &amp; Stirling Road</mail_address1>
 <mail_address2></mail_address2>
 <mail_suburb>Metung</mail_suburb>
 <mail_state>VIC</mail_state>
 <mail_postcode>3904</mail_postcode>
 <mail_country>Australia</mail_country>
 <email></email>
 <phone>03 5156 2306</phone>
 <mobile></mobile>
 <fax></fax>
 <latitude>-37.8833330</latitude>
 <longitude>147.8500000</longitude>
 
 */





/*
<?xml version="1.0" encoding="UTF-8" ?>
<root>
<product>
<id>1028541</id>
<merchant_id>25187</merchant_id>
<user_id>0</user_id>
<name>Avis 5th Day FREE!*</name>
<details>&lt;p&gt;Don&apos;t miss Avis&apos; exclusive offer for My Rewards members during November &amp;amp; December.&lt;/p&gt;
&lt;p&gt;Take advantage of the 5th day free* offer today!&lt;/p&gt;
</details>
<highlight>5th Day FREE* with Avis</highlight>
<text>&lt;p&gt;&lt;strong&gt;OFFER:&lt;/strong&gt; Rent an Intermediate, Standard, Full Size or Full Size Sports car from Avis in Australia for 5 consecutive days or more and get the 5th day free* of the time and kilometre charges.&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;REWARD ME:&lt;/strong&gt; &lt;span style=&quot;color: #00ccff;&quot;&gt;&lt;a href=&quot;http://www.avis.com.au/car-rental/deals/deals-au-AU_en-022694/deals-au-AU_en-022695/myrewards-5thday-AU_en-038363&quot; target=&quot;_blank&quot;&gt;&lt;span style=&quot;color: #00ccff;&quot;&gt;Click here&lt;/span&gt;&lt;/a&gt;&lt;/span&gt; to book this great offer and place in the promo code &lt;strong&gt;TPPA109&lt;/strong&gt; and the AWD code &lt;strong&gt;P620902&lt;/strong&gt; to redeem the discount.&lt;/p&gt;
&lt;p&gt;Terms &amp;amp; Conditions: Offer valid on rentals commenced by &lt;strong&gt;22 March 2013&lt;/strong&gt;. Black out period applies. &lt;span style=&quot;color: #00ccff;&quot;&gt;&lt;a href=&quot;http://www.avis.com.au/car-rental/deals/deals-au-AU_en-022694/deals-au-AU_en-022695/myrewards-5thday-AU_en-038363&quot; target=&quot;_blank&quot;&gt;&lt;span style=&quot;color: #00ccff;&quot;&gt;Click here&lt;/span&gt;&lt;/a&gt;&lt;/span&gt; to view the full terms &amp;amp; conditions of rental.&lt;/p&gt;
</text>
<terms_and_conditions>&lt;p&gt;Terms &amp;amp; Conditions: Offer valid on rentals commenced by &lt;strong&gt;22 March 2013&lt;/strong&gt;. Black out period applies. &lt;span style=&quot;color: #00ccff;&quot;&gt;&lt;a href=&quot;http://www.avis.com.au/car-rental/deals/deals-au-AU_en-022694/deals-au-AU_en-022695/myrewards-5thday-AU_en-038363&quot; target=&quot;_blank&quot;&gt;&lt;span style=&quot;color: #00ccff;&quot;&gt;Click here&lt;/span&gt;&lt;/a&gt;&lt;/span&gt; to view the full terms &amp;amp; conditions of rental.&lt;/p&gt;
</terms_and_conditions>
<keyword>&lt;p&gt;avis, rent, car, rental, holiday, corporate, day, trip, special, Avis&lt;/p&gt;
</keyword>
<link1></link1>
<link1_text></link1_text>
<link2></link2>
<link2_text></link2_text>
<image_extension></image_extension>
<display_image>Merchant Logo</display_image>
<active>1</active>
<special_offer>1</special_offer>
<special_offer_headline>5th Day FREE* with Avis</special_offer_headline>
<special_offer_body>&lt;p&gt;&lt;strong&gt;OFFER:&lt;/strong&gt; Rent an Intermediate, Standard, Full Size or Full Size Sports car from Avis in Australia for 5 consecutive days or more and get the 5th day free* of the time and kilometre charges.&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;REWARD ME:&lt;/strong&gt; &lt;span style=&quot;color: #00ccff;&quot;&gt;&lt;a href=&quot;http://www.avis.com.au/car-rental/deals/deals-au-AU_en-022694/deals-au-AU_en-022695/myrewards-5thday-AU_en-038363&quot; target=&quot;_blank&quot;&gt;&lt;span style=&quot;color: #00ccff;&quot;&gt;Click here&lt;/span&gt;&lt;/a&gt;&lt;/span&gt; to book this great offer and place in the promo code &lt;strong&gt;TPPA109&lt;/strong&gt; and the AWD code &lt;strong&gt;P620902&lt;/strong&gt; to redeem the discount.&lt;/p&gt;
&lt;p&gt;Terms &amp;amp; Conditions: Offer valid on rentals commenced by &lt;strong&gt;22 March 2013&lt;/strong&gt;. Black out period applies. &lt;span style=&quot;color: #00ccff;&quot;&gt;&lt;a href=&quot;http://www.avis.com.au/car-rental/deals/deals-au-AU_en-022694/deals-au-AU_en-022695/myrewards-5thday-AU_en-038363&quot; target=&quot;_blank&quot;&gt;&lt;span style=&quot;color: #00ccff;&quot;&gt;Click here&lt;/span&gt;&lt;/a&gt;&lt;/span&gt; to view the full terms &amp;amp; conditions of rental.&lt;/p&gt;
</special_offer_body><type>offer</type><price>0.00</price><quantity>0</quantity><expires>0000-00-00</expires><changed_expire>0000-00-00 00:00:00</changed_expire><show_and_save>0</show_and_save><book_coupon>0</book_coupon><web_coupon>0</web_coupon><mobile_reward>0</mobile_reward><phone_benefit>0</phone_benefit><online_purchase>0</online_purchase><available_national>1</available_national><available_act>0</available_act><available_nsw>0</available_nsw><available_nt>0</available_nt><available_qld>0</available_qld><available_sa>0</available_sa><available_tas>0</available_tas><available_vic>0</available_vic><available_wa>0</available_wa><search_weight>5</search_weight><assignment_override>0</assignment_override><views>107</views><created>0000-00-00 00:00:00</created><modified>2012-07-24 19:12:41</modified><cancelled>0</cancelled><renewed>0</renewed><yearuntil></yearuntil><used_count>-1</used_count><coupon_expiry>30</coupon_expiry><hotoffer>0</hotoffer><favourites>0</favourites><hotoffer_extension></hotoffer_extension><offer_start>2012-11-02</offer_start><mid>25187</mid><agreement_type_id>14</agreement_type_id><mname>AVIS</mname><contact_first_name>Hoang</contact_first_name><contact_last_name>Nguyen</contact_last_name><contact_title></contact_title><contact_position>Marketing Assistant</contact_position><mail_address1>Australia Wide Offer</mail_address1><mail_address2></mail_address2><mail_suburb></mail_suburb><mail_state></mail_state><mail_postcode></mail_postcode><mail_country>Australia</mail_country><email></email><phone>13 63 33</phone><mobile></mobile><fax></fax><latitude>-37.4505819</latitude><longitude>135.6037877</longitude><logo_extension>gif</logo_extension><m_image_extension>gif</m_image_extension><mtext></mtext></product></root>
*/