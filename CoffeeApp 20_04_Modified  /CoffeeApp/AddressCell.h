//
//  AddressCell.h
//  PDKTStickySectionHeadersCollectionViewLayoutDemo
//
//  Created by vairat on 16/03/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddressCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UIButton *mapButton;

@end
