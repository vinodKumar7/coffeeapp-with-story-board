//
//  LoyalityTableViewCell.h
//  PDKTStickySectionHeadersCollectionViewLayoutDemo
//
//  Created by vairat on 14/03/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoyalityTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblSubTitle;
@property (strong, nonatomic) IBOutlet UIImageView *product_ImageView;



@end
